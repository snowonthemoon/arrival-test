#!/usr/bin/python
# -*- coding: utf-8 -*-
import json
import copy
import asyncio
import aioconsole


#==========================================================================================================================
class VoiceInputModule:

    def __init__(self):
        pass

    async def GetInput(self):
        ret = await aioconsole.ainput() 
        return ret.lower()

#==========================================================================================================================
class VoiceOutputModule:

    def __init__(self):
        self.lastMessageSent = None         # для возможных юнит-тестов

    # Заглушка для "отправки" сообщения на модуль речевого вывода
    def SendMessage(self, message: dict):
        self.lastMessageSent = copy.deepcopy(message)
        print(f"VOICE OUTPUT : {message}")

#==========================================================================================================================
class DisplayModule:

    def __init__(self):
        self.lastOrderDisplayed = None          # для возможных юнит-тестов

    # Заглушка для "вывода" сообщения на дисплей
    def DisplayOrder(self, order: dict):
        self.lastOrderDisplayed = copy.deepcopy(order)
        print(f"DISPLAY : {order}")

#==========================================================================================================================
class ServerProcessingModule:

    def __init__(self):
        self.lastOrder = None           # для возможных юнит-тестов

    # Имитация отправки заказа на сервер
    def SendOrderToServer(self, order: dict):
        self.lastOrder = copy.deepcopy(order)
        print(f"SERVER SEND : {order}")



if __name__ == '__main__':
    pass