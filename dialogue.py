#!/usr/bin/python
# -*- coding: utf-8 -*-
import json
import time
import asyncio
from difflib import SequenceMatcher
from additional_modules import VoiceInputModule, VoiceOutputModule, DisplayModule, ServerProcessingModule

#==========================================================================================================================
# Config

# States
IDLE = 0
CHECK_OR = 1
CHECK_YN = 2
FINISH = 3
WD = 4

# Wdt timer constants
WD_QUESTION_SEC = 20
WD_CLEAR_SEC = 40

# Languages
EN = "EN"
RU = "RU"
GE = "GE"

# Keys for json vocabluary
VOC_FILENAME = "vocabluary.json"
DISHES = "Dishes"
NUMBERS = "Numbers"
PHRASES = "Phrases"

# Phrases keys
PHRASE_CONFIRM = "confirm"
PHRASE_DECLINE = "decline"
PHRASE_MORE = "more"
PHRASE_CONTINUE = "continue"
PHRASE_OR = "or"
PHRASE_YN = "yn"

# Probability consts
PROBABILITY_MAX = 1.0
PROBABILITY_BORDER = 0.81

# Question flags
QUESTION_NONE = 0
QUESTION_DISH = "DISH"
QUESTION_NUM = "NUM"

#==========================================================================================================================
class SpeechDetection:

    def __init__(self):
        self.__langDict = {}
        self.__coefMax = 0
        
    # Подсчёт коэффициентов для определения языка
    def __setLanguageCoef(self, lang, coef):
        if lang in self.__langDict.keys():
            self.__langDict[lang] += coef
        else:
            self.__langDict[lang] = coef

    # Возврат распознанного языка
    def GetLanguage(self, currentLang):
        language = currentLang
        coefMax = 0
        for lang in self.__langDict.keys():
            if self.__langDict[lang] > coefMax:
                coefMax = self.__langDict[lang]
                language = lang
        return language
    
    # Поиск вероятности вхождения строки vocString в строку inString
    def StringCheck(self, inString: str, vocString: str) -> float:
        coef = SequenceMatcher(a = inString, b = vocString).ratio()
        return coef

    # Точный поиск максимальной вероятности вхождения строки vocString в строку inString со сдвигом vocString по inString
    def StringCheckPrecise(self, inString: str, vocString: str) -> float:
        sizeString = len(inString)
        sizeVocArr = len(vocString)
        
        if sizeString < sizeVocArr:
            sizeString += (sizeVocArr - sizeString)
        coefSum = 0
        strList = inString.split()
        vocList = vocString.split()
        for i in vocList:
            coefMax = 0
            for j in strList:
                coef = SequenceMatcher(a = i, b = j).ratio()
                coefMax = max(coef, coefMax)
            coefSum += coefMax
        coefSum = coefSum/len(vocList)
        return coefSum

    # Коллбэк для заполнения выходного словаря при точном поиске попадания элемента в строку
    def StringPreciseCallback(self, inString: str, vocString: str, dct: dict, item: str):
        coef = self.StringCheckPrecise(inString, vocString)
        if coef >= PROBABILITY_BORDER: 
            if item not in dct.keys():
                dct[item] = coef
            elif dct[item] < coef:
                dct[item] = coef
            return coef
        else:
            return 0

    # Коллбэк для заполнения выходного словаря при уточнении одной позиции из набора нескольких
    def MaxSearchCallback(self, inString: str, vocString: str, dct: dict, item: str):  
        coef = self.StringCheck(inString, vocString)
        if coef > self.__coefMax:
            self.__coefMax = coef
            dct.clear()
            dct[item] = coef
        return coef

    # Функция для поиска элементов из словаря voc, входящих в строку inString с вероятностью выше probBorder
    # Параметры поиска также зависят от callback-функции, определяющ кейоэффициент strCheckCallback.
    # Поиск также может быть сужен до конкретного языка (конфигурацией аргумента inLang) или конкретного набора элементов (конфигурацией аргумента inItem)
    def VocProcessing(self, inString: str, voc: dict, CallbackFunc, inItem: list = None, inLang: list = None) -> dict:
        retDict = {}
        if inItem == None:
            itemList = voc.keys()
        else:
            itemList = inItem
        for item in itemList:
            if inLang == None:
                langList = voc[item].keys()
            else:
                langList = inLang
            for lang in langList:
                for i in voc[item][lang]:
                    coef = CallbackFunc(inString, i, retDict, item)
                    self.__setLanguageCoef(lang, coef/len(voc[item][lang])) 
        return retDict

#==========================================================================================================================
class Order:
    def __init__(self):
        self.__dish = None
        self.__num = None       
        self.questionsCnt = 0
        self.questionFlag = QUESTION_NONE
        self.dishCheckList = []
        self.numCheckList = []
    
    # Конфигурация состояния заказа на основании полученных после распознования речи блюд
    def SetDishBuffer(self, inBuffer: dict):
        keys = list(inBuffer.keys())
        if len(keys) == 1:
            if inBuffer[ keys[0] ] == PROBABILITY_MAX:
                self.__dish = keys[0]
                self.dishCheckList = []
            else:
                self.dishCheckList = keys
        elif len(keys) > 1:
            self.dishCheckList = keys

    # Конфигурация состояния заказа на основании полученных после распознования речи чисел
    def SetNumBuffer(self, inBuffer: dict):
        keys = list(inBuffer.keys())
        if len(keys) == 1:
            if inBuffer[ keys[0] ] == PROBABILITY_MAX:
                self.__num = keys[0]
                self.numCheckList = []
            else:
                self.numCheckList = keys
        elif len(keys) > 1:
            self.numCheckList = keys
    
    # Функция обновления данных блюда в заказе после вопроса "Да/Нет"
    def CheckYnDish(self, success: bool):
        if success == True:
            self.__dish = self.dishCheckList[0]

    # Функция обновления данных числа в заказе после вопроса "Да/Нет"
    def CheckYnNum(self, success: bool):
        if success == True:
            self.__num = self.numCheckList[0]

    # Возвращает текущее блюдо в данной позиции заказа
    def GetDish(self) -> str:
        return self.__dish

    # Возвращает текущее число в данной позиции заказа
    def GetNum(self) -> str:
        return self.__num

    # Очищает поля заказа
    def Clear(self):
        self.__dish = None
        self.__num = None       
        self.questionsCnt = 0
        self.questionFlag = QUESTION_NONE
        self.dishCheckList = []
        self.numCheckList = []

#==========================================================================================================================
class Dialogue:

    def __init__(self, voiceTx: VoiceOutputModule, display: DisplayModule, server: ServerProcessingModule):
        self.__state = IDLE
        self.__language = EN
        self.__wdFlag = False
        self.__wdTime = time.time()
        self.__orderObj = Order()

        self.currentOrderCnt = 0
        self.currentOrderDict = {}
        self.voiceOut = voiceTx
        self.displayOut = display
        self.serverOut = server

        f = open(VOC_FILENAME, 'r', encoding = 'utf-8')
        vocabluary = json.load(f)
        self.vocDishes = vocabluary[DISHES]
        self.vocNumbers = vocabluary[NUMBERS]
        self.vocPhrases = vocabluary[PHRASES]
        f.close

    # Интерфейс работы с голосовым выводом
    def __sendVoiceMessage(self, itemsList: list, language: str):
        messageDict = {"Message": itemsList, "Language": language}
        self.voiceOut.SendMessage(messageDict)

    # Интерфейс работы с выводом на дисплей
    def __displayOrder(self, orderList: list):
        orderDict = {str(self.currentOrderCnt) : orderList}
        self.displayOut.DisplayOrder(orderDict)

    # Интерфейс работы с сервером
    def __sendOrderToServer(self, orderList: list):
        orderDict = {str(self.currentOrderCnt) : orderList}
        self.serverOut.SendOrderToServer(orderDict)
        
    # Функция отправки вопроса через модуль голосового вывода
    def __question(self, order: Order, newState: int, *args):
        if order.questionsCnt >= 2:
            self.__finishRequest(order, False)
            return
        self.__sendVoiceMessage( list(args), self.__language )
        order.questionsCnt += 1
        self.__state = newState

    # Функция обработки перехода к завершению приема позиции заказа
    def __finishRequest(self, order: Order, success: bool):
        self.__state = FINISH
        if success == True:
            dish = order.GetDish()
            if order.GetDish() not in self.currentOrderDict.keys():
                self.currentOrderDict[ dish ] = order.GetNum() 
            else:
                self.currentOrderDict[ dish ] = int(self.currentOrderDict[ dish ]) + int(order.GetNum())
        self.__displayOrder(self.currentOrderDict)
        self.__sendVoiceMessage( [PHRASE_MORE], self.__language )
        self.__orderObj.Clear()

    # Функция обработки завершения приема заказа
    def __finishProcesshing(self):
        if len(self.currentOrderDict) > 0:
            self.__sendOrderToServer(self.currentOrderDict)
            self.currentOrderCnt += 1
            self.currentOrderDict.clear()

    # Функция очистки заказа и отката в начальное состояние
    def __clearOrder(self):
        self.__state = IDLE
        self.currentOrderDict.clear()
        self.__displayOrder(self.currentOrderDict)
                
    # Обработка текущего состояния позиции заказа с условиями для уточняющих вопросов
    def __checkProcessing(self, order: Order):

        if (order.GetDish() != None) and (order.GetNum() != None):
            self.__finishRequest(order, True) 

        elif (order.GetDish() == None) and (order.GetNum() == None): 
            order.questionFlag = QUESTION_DISH
            if len(order.dishCheckList) > 1:             
                self.__question(order, CHECK_OR, PHRASE_OR, *order.dishCheckList)
            elif len(order.dishCheckList) == 1:
                self.__question(order, CHECK_YN, PHRASE_YN, *order.dishCheckList)
            else:
                pass

        elif (order.GetDish() == None) and (order.GetNum() != None): 
            order.questionFlag = QUESTION_DISH
            if len(order.dishCheckList) > 1:             
                self.__question(order, CHECK_OR, PHRASE_OR, *order.dishCheckList, order.GetNum())
            elif len(order.dishCheckList) == 1:
                self.__question(order, CHECK_YN, PHRASE_YN, *order.dishCheckList, order.GetNum())
            else:
                pass

        elif order.GetNum() == None:
            order.questionFlag = QUESTION_NUM
            if len(order.numCheckList) > 1:
                self.__question(order, CHECK_OR, PHRASE_OR, order.GetDish(), *order.numCheckList)
            elif len(order.numCheckList) == 1:
                self.__question(order, CHECK_YN, PHRASE_YN, order.GetDish(), *order.numCheckList)
            else:
                order.SetNumBuffer({'1': PROBABILITY_BORDER})
                self.__question(order, CHECK_YN, PHRASE_YN, order.GetDish(), *order.numCheckList)
        
    # Основная функция работы диалогового модуля 
    def DialogueTask(self, inString: str):
        spch = SpeechDetection()
        self.__setWd()

        while(True):

            if self.__state == IDLE:
                buff = spch.VocProcessing(inString, self.vocDishes, spch.StringPreciseCallback)
                self.__orderObj.SetDishBuffer(buff)
                buff = spch.VocProcessing(inString, self.vocNumbers, spch.StringPreciseCallback)
                self.__orderObj.SetNumBuffer(buff)
                self.__language = spch.GetLanguage(self.__language)

                self.__checkProcessing(self.__orderObj)
                break
                
            elif(self.__state == CHECK_OR):
                if (self.__orderObj.questionFlag == QUESTION_DISH):
                    vocabluary = self.vocDishes
                    itemList = self.__orderObj.dishCheckList
                    callback = self.__orderObj.SetDishBuffer
                elif (self.__orderObj.questionFlag == QUESTION_NUM):
                    vocabluary = self.vocNumbers
                    itemList = self.__orderObj.numCheckList
                    callback = self.__orderObj.SetNumBuffer
                else:
                    break

                buff = spch.VocProcessing(inString, vocabluary, spch.MaxSearchCallback, 
                                                                inItem = itemList )
                buff = spch.VocProcessing(inString, vocabluary, spch.StringPreciseCallback,  
                                                                inItem = list(buff.keys()) )
                self.__language = spch.GetLanguage(self.__language)

                callback(buff)
                self.__checkProcessing(self.__orderObj)
                break

            elif(self.__state == CHECK_YN):
                if (self.__orderObj.questionFlag == QUESTION_DISH):
                    callback = self.__orderObj.CheckYnDish
                elif (self.__orderObj.questionFlag == QUESTION_NUM):
                    callback = self.__orderObj.CheckYnNum
                else:
                    break
                buff = spch.VocProcessing(inString, self.vocPhrases, spch.StringPreciseCallback,  
                                                                inItem = [PHRASE_CONFIRM, PHRASE_DECLINE] )         
                self.__language = spch.GetLanguage(self.__language)

                if PHRASE_DECLINE in buff.keys():
                    callback(False)
                    self.__finishRequest(self.__orderObj, False)
                    break
                elif PHRASE_CONFIRM in buff.keys():
                    callback(True)

                self.__checkProcessing(self.__orderObj)
                break

            elif(self.__state == FINISH):       
                buff = spch.VocProcessing(inString, self.vocPhrases, spch.StringPreciseCallback, 
                                                                inItem = [PHRASE_CONFIRM, PHRASE_DECLINE] )               
                self.__language = spch.GetLanguage(self.__language)

                self.__state = IDLE
                if PHRASE_DECLINE in buff.keys():
                    self.__finishProcesshing()
                elif PHRASE_CONFIRM in buff.keys():
                    pass
                else:
                    continue

                break

            elif(self.__state == WD):
                buff = spch.VocProcessing(inString, self.vocPhrases, spch.StringPreciseCallback, 
                                                                inItem = [PHRASE_CONFIRM, PHRASE_DECLINE] ) 
                self.__language = spch.GetLanguage(self.__language)
  
                if PHRASE_DECLINE in buff.keys():
                    self.__clearOrder()
                else:
                    self.__orderObj.questionsCnt = 0
                    self.__state = IDLE
                    self.__checkProcessing(self.__orderObj)

                break

    # Watchdog на случай
    def __setWd(self):
        self.__wdTime = time.time()
        self.__wdFlag = False

    # Проверка состояния watchdog-а и возврат в IDLE состояние в случае 
    async def TimerTask(self):
        await asyncio.sleep(1)
        if self.__state == IDLE and len(self.currentOrderDict) == 0:
            return

        if (time.time() - self.__wdTime) > WD_QUESTION_SEC:
            if self.__wdFlag == False:
                self.__wdFlag = True
                self.__state = WD
                self.__sendVoiceMessage([PHRASE_CONTINUE], self.__language)

        if (time.time() - self.__wdTime) > WD_CLEAR_SEC:
            self.__clearOrder()

            


            
#=========================================================================================================================

async def DialogueMainTask(dialogue, input):
    while True:
        inString = await input.GetInput()
        dialogue.DialogueTask(inString)

async def DialogueTimerTask(dialogue):
    while True:
        await dialogue.TimerTask()


if(__name__ == "__main__"):

    ioloop = asyncio.get_event_loop()
    

    voiceIn = VoiceInputModule()
    voiceOut = VoiceOutputModule()
    display = DisplayModule()
    server = ServerProcessingModule()
    dlg = Dialogue(voiceOut, display, server)
    tasks = [ ioloop.create_task( DialogueMainTask(dlg, voiceIn) ), ioloop.create_task( DialogueTimerTask(dlg) ) ]
    wait_tasks = asyncio.wait(tasks)
    ioloop.run_until_complete(wait_tasks)
    ioloop.close()